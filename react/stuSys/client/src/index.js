import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {BrowserRouter} from 'react-router-dom'
// import {store} from './redux/store'
import storeRedux from './react-redux/store'
import { Provider } from 'react-redux';

const root = ReactDOM.createRoot(document.getElementById('root'));

function render(){
  root.render(
    <Provider store={storeRedux}>
      <BrowserRouter>
        <App/>
      </BrowserRouter>
    </Provider>
  );
}

render()

// store.subscribe(render)
