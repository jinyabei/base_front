import {ADD,DEL,CHANGE} from "./actionType"

let defaultState = {
  list: [
    {
      content: "学习 React",
      status: false,
    },
    {
      content: "复习 Vue",
      status: false,
    },
    {
      content: "玩游戏",
      status: false,
    },
    {
      content: "听歌",
      status: false,
    },
  ], 
}

export function todoReducer(state=defaultState,action){
  switch(action.type){
    case ADD:{
      const arr = [...state.list]
      arr.push({
        content:action.data,
        status:false,
      })
      return {list:arr}
    }
    case DEL:{
      const arr = [...state.list]
      arr.splice(action.data,1);
      return {list:arr}
    }
    case CHANGE:{
      const arr = [...state.list]
      arr[action.data].status = !arr[action.data].status
      return {list:arr};
    }
    default: return state
  }
}