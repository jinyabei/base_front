import { useDispatch, useSelector } from "react-redux"
import {del,change} from "../../react-redux/todolistSlice"

const List = ()=>{
  const dispatch = useDispatch()
  const {list} = useSelector(state=>state.todo);

  const lis = list.map((item,index)=>{
    return (
      <li key={index}>
        <span 
          onClick={()=>dispatch(change(index))}
          className={["item",item.status?"completed":""].join(" ")}>{item.content}</span>
        <button 
          type="button" 
          className="close" 
          onClick={()=>dispatch(del(index))}>&times;</button>
      </li>
    )
  })
  return(
    <div>
      <ul>
        {lis}
      </ul>
    </div>
  )
}

export default List