import { useState } from "react"
import { add } from "../../react-redux/todolistSlice";
import { useDispatch } from "react-redux";

const Input = ()=>{
  const [value,setValue] = useState("");

  const dispatch = useDispatch()

  const clickHandle = ()=>{
    dispatch(add(value))
    setValue('')
  }

  return(
    <div className="form-inline">
      <input 
        type="text"
        placeholder="请输入代办事项"
        className="form-control"
        style={{
          marginRight:10
        }}
        value={value}
        onChange={(e)=>setValue(e.target.value)}
      />
      <button className="btn btn-primary" onClick={clickHandle}>提交</button>
    </div>
  )
}

export default Input