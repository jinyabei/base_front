import Input from './Input'
import List from './Form'

const Todo = (props)=>{
  return(
    <div className="container">
        <h1 className="lead" style={{marginBottom:'30px'}}>代办事项</h1>
        <Input store={props.store} />
        <List store={props.store} />
    </div>
  )
}

export default Todo