import { useState } from "react"
import { addListAction } from "../../redux/actions";

const Input = (props)=>{
  const [value,setValue] = useState("");

  const clickHandle = ()=>{
    props.store.dispatch(addListAction(value))
    setValue('')
  }

  return(
    <div className="form-inline">
      <input 
        type="text"
        placeholder="请输入代办事项"
        className="form-control"
        style={{
          marginRight:10
        }}
        value={value}
        onChange={(e)=>setValue(e.target.value)}
      />
      <button className="btn btn-primary" onClick={clickHandle}>提交</button>
    </div>
  )
}

export default Input