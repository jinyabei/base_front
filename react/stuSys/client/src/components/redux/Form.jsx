import { changeAction, delListAction } from "../../redux/actions"

const List = (props)=>{
  const lis = props.store.getState().list.map((item,index)=>{
    return (
      <li key={index}>
        <span 
          onClick={()=>props.store.dispatch(changeAction(index))}
          className={["item",item.status?"completed":""].join(" ")}>{item.content}</span>
        <button 
          type="button" 
          className="close" 
          onClick={()=>props.store.dispatch(delListAction(index))}>&times;</button>
      </li>
    )
  })
  return(
    <div>
      <ul>
        {lis}
      </ul>
    </div>
  )
}

export default List