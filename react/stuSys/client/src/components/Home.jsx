import { useState, useEffect } from "react";
// import { getStuListApi } from "../api/stuApi";
import { useLocation,NavLink } from 'react-router-dom'
import Alert from "./Alert"
import { useDispatch, useSelector } from "react-redux";
import { getStuListAsync } from "../react-redux/stuSlice";

export default function Home() {

  // 组件数据
  // const [stuList, setStuList] = useState([]); // 存储完整的用户列表数据

  const {stuList} = useSelector(state=>state.stu);

  const [searchItem, setSearchItem] = useState(""); // 搜索数据
  const [alert, setAlert] = useState(null); // 存储提示信息
  const [searchList, setSearchList] = useState([]); // 存储搜索结果

  const location = useLocation()
  const dispatch = useDispatch()

  // 获取用户列表
  useEffect(() => {
    // getStuListApi().then(({ data }) => {
    //   setStuList(data);
    // })
    if(!stuList.length){
      dispatch(getStuListAsync())
    }
  }, [stuList,dispatch]);

  // 再来一个副作用，用于获取跳转到 Home 组件时传递的 state 数据
  useEffect(() => {
    if(location.state){
      setAlert(location.state)
    }
  }, [location]);

  const showAlert = alert ? <Alert {...alert} /> : null;

  // 搜索功能
  function changeHandle(name) {
    // 同步更新搜索框
    setSearchItem(name);
    // 简单的按照姓名进行过滤
    const arr = stuList.filter((item) => {
      return item.name.match(name);
    });
    setSearchList(arr);
  }

  const list = searchItem ? searchList : stuList;

  const trs = list.map((item, index) => {
    return (
      <tr key={index}>
        <td>{item.name}</td>
        <td>{item.age}</td>
        <td>{item.phone}</td>
        <td>{item.email}</td>
        <td>{item.education}</td>
        <td>{item.graduationschool}</td>
        <td>{item.profession}</td>
        <td>
          <NavLink to={`/detail/${item.id}`}>详情</NavLink>
        </td>
      </tr>
    );
  });

  return (
    <div className="users">
      {showAlert}
      <h1 className="page-header">用户列表</h1>
      <input type="text"
        className="form-control"
        placeholder="搜索"
        onChange={(e) => changeHandle(e.target.value)}
        value={searchItem}
      />
        <table className="table table-striped table-bordered">
          <thead>
            <tr>
              <th>姓名</th>
              <th>年龄</th>
              <th>电话</th>
              <th>邮箱</th>
              <th>文化水平</th>
              <th>毕业院校</th>
              <th>专业</th>
              <th>更多信息</th>
            </tr>
          </thead>
          <tbody>
            {trs}
          </tbody>
        </table>
    </div>
  );
}