import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
// import { getStuByIdApi, deleteStuByIdApi } from "../api/stuApi.js"
import { useDispatch, useSelector } from "react-redux";
import { deleteStuAsync } from "../react-redux/stuSlice";

export default function Detail(props) {
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const {stuList} = useSelector(state=>state.stu)

  // 组件状态
  const [stu, setStu] = useState({
    name: '',
    age: '',
    phone: '',
    email: '',
    education: '',
    graduationschool: '',
    profession: '',
    profile: ''
  });


  useEffect(() => {
    // getStuByIdApi(id).then(({ data }) => {
    //   setStu(data);
    // })
    const curStu = stuList.filter(stu=>stu.id===id);
    setStu(curStu[0])
  }, [id,stuList]);


  function deleteStu(id) {
    if (window.confirm('你确定要删除此名用户么？')) {
      dispatch(deleteStuAsync(id));
      navigate("/home",{
        state: {
          alert: "用户删除成功！",
          type : "info"
        }
      });
      // deleteStuByIdApi(id).then(() => {
      //   navigate("/home",{
      //     state: {
      //       alert: "用户删除成功！",
      //       type : "info"
      //     }
      //   });
      // })
    }
  }

  return (
      <div className="details container">
          <button className="btn btn-default" onClick={() => navigate("/home")}>返回</button>
          <h1 className="page-header">
              {stu.name}
              <span className="pull-right">
                  <button className="btn btn-primary" onClick={() => navigate(`/edit/${stu.id}`)} style={{ marginRight: 10 }}>修改</button>
                  <button className="btn btn-danger" onClick={() => deleteStu(stu.id)}>删除</button>
              </span>
          </h1>
          {/* 第一组 */}
          <ul className="list-group">
              <li className="list-group-item">
                  <span className="glyphicon glyphicon-phone">电话：{stu.phone}</span>
              </li>
              <li className="list-group-item">
                  <span className="glyphicon glyphicon-envelope">邮箱：{stu.email}</span>
              </li>
          </ul>
          {/* 第二组 */}
          <ul className="list-group">
              <li className="list-group-item">
                  <span className="glyphicon glyphicon-book">文化水平：{stu.education}</span>
              </li>
              <li className="list-group-item">
                  <span className="glyphicon glyphicon-flag">毕业院校：{stu.graduationschool}</span>
              </li>
              <li className="list-group-item">
                  <span className="glyphicon glyphicon-briefcase">专业：{stu.profession}</span>
              </li>
              <li className="list-group-item">
                  <span className="glyphicon glyphicon-user">个人简介：{stu.profile}</span>
              </li>
          </ul>
      </div>
  );
}