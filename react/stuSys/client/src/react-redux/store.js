import { configureStore } from "@reduxjs/toolkit";
import todolistReducer from "./todolistSlice";
import stuReducer from "./stuSlice";

const storeRedux = configureStore({
  reducer:{
    todo:todolistReducer,
    stu:stuReducer
  }
})

export default storeRedux