import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { deleteStuByIdApi, getStuListApi, addStuApi, editStuByIdApi, } from "../api/stuApi";

export const getStuListAsync = createAsyncThunk(
  "stu/getStuListAsync",
  async (_, thunkApi) => {
    // 发送 ajax 请求
    const response = await getStuListApi();
    // 派发 action
    thunkApi.dispatch(initStuList(response.data));
  }
)

export const deleteStuAsync = createAsyncThunk(
  "stu/deleteStuAsync",
  async (payload, thunkApi) => {
    // 和服务器进行通信，删除对应 id 的学生
    deleteStuByIdApi(payload);
    thunkApi.dispatch(deleteStu(payload));
  }
)

// 异步新增学生
export const addStuAsync = createAsyncThunk(
  "stu/addStuAsync",
  async (payload, thunkApi) => {
    const { data } = await addStuApi(payload);
    // 将这个 data 更新到数据仓库
    thunkApi.dispatch(addStu(data));
  }
);

// 异步修改学生
export const editStuAsync = createAsyncThunk(
  "stu/editStuAsync",
  async (payload, thunkApi) => {
    editStuByIdApi(payload.id, payload.stu);
    thunkApi.dispatch(editStu(payload));
  }
);


export const stuSlice = createSlice({
  name:'stu',
  initialState:{
    stuList:[]
  },
  reducers:{
    initStuList:(state,{payload})=>{
      state.stuList = payload;
    },
    deleteStu:(state,{payload})=>{
      for(let i=0;i<state.stuList.length;i++){
        if(state.stuList[i].id===payload){
          state.stuList.splice(i,1);
          break;
        }
      }
    },
    addStu:(state,{payload})=>{
      state.stuList.push(payload)
    },
    editStu:(state,{payload})=>{
      for(let i=0;i<state.stuList.length;i++){
        if(state.stuList[i].id===payload.id){
          state.stuList.splice(i,1,payload.stu);
          break;
        }
      }  
    },
  }
})

const {initStuList,deleteStu,addStu,editStu} = stuSlice.actions
export default stuSlice.reducer