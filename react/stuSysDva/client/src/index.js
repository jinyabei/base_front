import dva from "dva";
import './index.css';

import {createBrowserHistory as createHistory} from "history";


const app = dva({
  history:createHistory()
})

//定义model
app.model(require('./models/stuModel').default)

//定义路由
app.router(require('./router').default)

app.start("#root")