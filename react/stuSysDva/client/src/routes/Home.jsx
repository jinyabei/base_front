import { useState, useEffect } from "react";
import { NavLink } from "dva/router";
import { connect } from "dva";
import Alert from "../components/Alert"

function Home(props) {

  const [searchItem, setSearchItem] = useState(""); // 搜索数据
  const [alert, setAlert] = useState(null); // 存储提示信息
  const [searchList, setSearchList] = useState([]); // 存储搜索结果

   // 执行副作用
  // 获取提示信息
  useEffect(() => {
    const {location} = props
    if(location.query){
      setAlert(location.query)
    }
  }, [props]);

  useEffect(() => {
    props.dispatch({
      type: "stuModel/_getStuList"
    });
  }, [props]);

  const showAlert = alert ? <Alert {...alert} /> : null;

  // 搜索功能
  function changeHandle(name) {
    // 同步更新搜索框
    setSearchItem(name);
    // 简单的按照姓名进行过滤
    const arr = props.stuList.filter((item) => {
      return item.name.match(name);
    });
    setSearchList(arr);
  }

  const list = searchItem ? searchList : props.stuList;

  const trs = list.map((item, index) => {
    return (
      <tr key={index}>
        <td>{item.name}</td>
        <td>{item.age}</td>
        <td>{item.phone}</td>
        <td>{item.email}</td>
        <td>{item.education}</td>
        <td>{item.graduationschool}</td>
        <td>{item.profession}</td>
        <td>
          <NavLink to={`/detail/${item.id}`}>详情</NavLink>
        </td>
      </tr>
    );
  });

  return (
    <div className="users">
      {showAlert}
      <h1 className="page-header">用户列表</h1>
      <input type="text"
        className="form-control"
        placeholder="搜索"
        onChange={(e) => changeHandle(e.target.value)}
        value={searchItem}
      />
        <table className="table table-striped table-bordered">
          <thead>
            <tr>
              <th>姓名</th>
              <th>年龄</th>
              <th>电话</th>
              <th>邮箱</th>
              <th>文化水平</th>
              <th>毕业院校</th>
              <th>专业</th>
              <th>更多信息</th>
            </tr>
          </thead>
          <tbody>
            {trs}
          </tbody>
        </table>
    </div>
  );
}

const mapStateToProps = (state)=>{
  return{
    stuList:state.stuModel.stuList
  }
}

export default connect(mapStateToProps)(Home)